CREATE OR REPLACE FUNCTION prod.get_daily_spreads_sample (sourceid NUMBER,
									sinkid NUMBER,
									marketday DATE,
									loadid NUMBER default null)
	/*
	* DB Technical Interview | Code Review
	* This function returns spreads for the given marketday at 5 min intervals.
	* Spread = LMP at the sink - LMP at the source
	*
	* Sample call: select get_daily_spreads_sample(1, 237, date'2022-01-02') from dual;
	*
	*/
	RETURN SYS_REFCURSOR
 IS
	  c   SYS_REFCURSOR;
 BEGIN
	  OPEN c FOR
			with
				src
					as (select ps.*, 
									pn.pnodename, 
									dl.collectdate
						from prod.price_nodes_sample pn,
								prod.prices_sample ps,
								prod.data_log dl
						where pn.objectid (+) = ps.objectid
							and ps.loadid = dl.loadid
							and pn.objectid = get_daily_spreads_sample.sourceid
							--and pn.loadid = :loadid
							)
				,
				snk
					as (select ps.*, 
									pn.pnodename, 
									dl.collectdate
						from prod.price_nodes_sample pn,
								prod.prices_sample ps,
								prod.data_log dl
						where pn.objectid (+) = ps.objectid
							and pn.objectid = get_daily_spreads_sample.sinkid)			
				select 
						sr.datetime,
						sr.objectid as sourceid,
						sr.pnodename as sourcename,
						sn.objectid as sinkid,
						sn.pnodename as sinkname,
						(sr.lmp - sn.lmp) as spread		
				from src sr,
						snk sn
				where sr.datetime = sn.datetime
						and sr.datetime between get_daily_spreads_sample.marketday and trunc(get_daily_spreads_sample.marketday) + 1
				order by sr.datetime desc;

	  RETURN c;
 END;

