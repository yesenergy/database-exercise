# Yes Energy Database Technical Interview #

## Instructions ##
<p>You have been asked to do a code review for the function GET_DAILY_SPREADS.<br>
This function returns spreads for the given marketday at five minute intervals.<br><br>
A Spread is the price (LMP) at the sink minus the price (LMP) at the source.</p>

### Sample Call ###
select get_daily_spreads_sample(1, 237, date'2022-01-02') from dual;

## Functions ##
This directory has DDL script (get_daily_spreads.sql) for the function used in the exercise.<br>

## Tables ##
This directory has DDL scripts for the tables used in the exercise.<br>
<ul>
<li>DATA_LOG.sql<br>DDL for a logging table.<br><br></li>
<li>PRICES_SAMPLE.sql<br>DDL for a price data table.<br><br></li>
<li>PRICE_NODES_SAMPLE.sql<br>DDL for an object dimension table.<br></li>
</ul>

## Sample Data ##
This directory has sample data files for the tables listed above.<br>