CREATE OR REPLACE PACKAGE SAMPLE_PKG
AS
    FUNCTION get_price_nodes (nodename VARCHAR2)
        RETURN SYS_REFCURSOR;

    FUNCTION get_price_nodes_ (nodename VARCHAR2)
        RETURN ty_string_nt;

    FUNCTION get_plants_ (plantname VARCHAR2)
        RETURN ty_string_nt;

    FUNCTION get_plant_attrs_ (columnname VARCHAR2, plantname VARCHAR2)
        RETURN ty_string_nt;

    FUNCTION get_url (url VARCHAR2)
        RETURN CLOB;
END;
CREATE OR REPLACE PACKAGE BODY SAMPLE_PKG
AS
    FUNCTION get_price_nodes (nodename VARCHAR2)
        RETURN SYS_REFCURSOR
    IS
        c   SYS_REFCURSOR;
    BEGIN
        OPEN c FOR SELECT * FROM get_price_nodes_ (get_price_nodes.nodename);

        RETURN c;
    END;

    FUNCTION get_price_nodes_ (nodename VARCHAR2)
        RETURN ty_string_nt
    IS
        t   ty_string_nt;
    BEGIN
        SELECT pnodename
          BULK COLLECT INTO t
          FROM price_nodes
         WHERE pnodename LIKE get_price_nodes_.nodename;

        RETURN t;
    END;

    FUNCTION get_plants_ (plantname VARCHAR2)
        RETURN ty_string_nt
    IS
        c           SYS_REFCURSOR;
        t           ty_string_nt;
        sqlstring   VARCHAR2 (2000);
    BEGIN
        sqlstring := 'SELECT plant_name FROM plants WHERE plant_name like :p';


        OPEN c FOR sqlstring USING plantname;

        FETCH c BULK COLLECT INTO t;

        RETURN t;
    END;

    FUNCTION get_plant_attrs_ (columnname VARCHAR2, plantname VARCHAR2)
        RETURN ty_string_nt
    IS
        c           SYS_REFCURSOR;
        t           ty_string_nt;
        sqlstring   VARCHAR2 (2000);
    BEGIN
        sqlstring :=
            'SELECT <<COLUMN_NAME>> FROM plants WHERE plant_name like :p';
        sqlstring := REPLACE (sqlstring, '<<COLUMN_NAME>>', columnname);

        OPEN c FOR sqlstring USING plantname;

        FETCH c BULK COLLECT INTO t;

        RETURN t;
    END;

    FUNCTION get_url (url VARCHAR2)
        RETURN CLOB
    IS
    BEGIN
        RETURN (httpuritype(
                    'http://ets.aeso.ca/ets_web/ip/Market/Reports/CSDReportServlet').getclob());
    END;
END;